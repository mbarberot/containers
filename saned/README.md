Saned in Docker
================

How to use
----------

```bash
# Build image
docker build -t registry.gitlab.com/mbarberot/containers/sane:debian11-arm .

# Push image
docker login registry.gitlab.com
docker push registry.gitlab.com/mbarberot/containers/sane:debian11-arm

# Run image
docker run \
  -d \
  -it \
  --name sane \
  -v /var/run/dbus:/var/run/dbus \
  -v /dev/bus/usb:/dev/bus/usb \
  -p 10000:10000 \
  -p 10001:10001 \
  -p 6566:6566 \
  --privileged \
  --net=host \
  --restart=unless-stopped \
  registry.gitlab.com/mbarberot/containers/sane:debian11-arm
```

Automated build
---------------

```bash
# ./build.sh <TAG>
./build.sh debian12-arm
```

Links
-----

Using it @ home and I'm talking about it in my blog : [Serveur de numérisation (fr)](https://mbarberot.gitlab.io/posts/xsane-server/).  
Inspired by [mback2k/docker-saned-hp](https://github.com/mback2k/docker-saned-hp) (simplified to only embed saned).
