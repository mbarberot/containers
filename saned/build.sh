#!/usr/bin/env bash

TAG=${1}

if [[ "${TAG}" == "" ]]
then
	echo "$0 <TAG>"
	exit 1
fi

# Build
docker build -t registry.gitlab.com/mbarberot/containers/sane:${TAG} .

# Push image
docker login registry.gitlab.com
docker push registry.gitlab.com/mbarberot/containers/sane:${TAG}
