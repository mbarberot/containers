debian-jdk15
============

Build 
-----

The build command require `files/adoptopenjdk.gpg` to exists. You can get it by following these instruction from the [official website](https://adoptopenjdk.net/installation.html#linux-pkg).  

Build image using Docker : 
```bash 
docker build .
```

Pull
----

You can also pull an image from the project's registry.

